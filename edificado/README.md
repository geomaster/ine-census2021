# Instalar Plugin
O plugin para a comparação do edificado encontra-se disponível no Gitlab. Devemos começar por descarregar o zip com o plugin: https://gitlab.com/geomaster/ine-census2021/-/raw/master/edificado/edificadoINE.zip.

Depois vamos abrir o QGIS e selecionar o menu 'Plugins'>'Manage and Install Plugins...'

<div align="center">

![ine-plg-inst1.png](https://w.geomaster.pt/ine-plugin-edif/ine-plg-inst1.png)

</div>

Na janela seguinte selecionamos 'Install from ZIP', configuramos o caminho para o ficheiro `.zip` do plugin e carregamos no botão ''Install Plugin.

<div align="center">

![ine-plg-inst2.png](https://w.geomaster.pt/ine-plugin-edif/ine-plg-inst2.png)

</div>

Após a instalação é necessário ativar o plugin, para isso precisamos de voltar ao menu 'Plugins'>'Manage and Install Plugins...', selecionar 'Installed' e garantir que o plugin `edificadoINE` se encontra selecionado.

<div align="center">

![ine-plg-inst3.png](https://w.geomaster.pt/ine-plugin-edif/ine-plg-inst3.png)

</div>

# Comparar Edificado
Após instalar o plugin, começamos por fazer download da Base Geográfica de Edifícios (BGE) do INE para comparar com os dados locais. No menu 'Plugins' do QGIS, seleccionar o plugin edificadoINE e clicar na opção 'settings'.

<div align="center">

<!--![ine-plg-edif1.png](https://w.geomaster.pt/ine-plugin-edif/ine-plg-edif1.png =700x)-->
![ine-plg-edif1.png](https://w.geomaster.pt/ine-plugin-edif/ine-plg-edif1.png)

</div>

No menu settings vamos preencher os dados de autenticação do utilizador do município registado com o INE e proceder ao download da BGE clicando no butão 'Download BGE INE'.

<div align="center">

![ine-plg-edif2.png](https://w.geomaster.pt/ine-plugin-edif/ine-plg-edif2.png)

</div>

Após terminar o download a lista de camadas vai ser atualizada com um grupo chamado '\[INE\] Dados' onde vai estar a camada INE BGE com o edificado relevante e seus atributos.

## Configurar Comparação
Para proceder à comparação do edificado voltamos ao menu 'Plugins' > 'edificadoINE' e selecionamos a opção 'edificadoINE'. Isto vai abrir uma janela onde vão ser configurados os parâmetros a usar na comparação.

<div align="center">

![ine-plg-edif3.png](https://w.geomaster.pt/ine-plugin-edif/ine-plg-edif3.png)

</div>

A configuração mínima necessária para poder efetuar a comparação exige o preenchimento da secção 'Camadas', 'Morada', e do campo 'Chave Primária'.

Na secção camadas devemos configurar como camada 'Edifícios BGE/INE' a camada que fizemos download na secção anterior e colocar como 'Edifícios Municipio' a camada local referente ao edificado do município.

O atributo 'Chave Primária' deve ser configurado com o campo(s) relativo ao identificador dos edifícios na camada do edificado do município.

<div align="center">

![ine-plg-edif4.png](https://w.geomaster.pt/ine-plugin-edif/ine-plg-edif4.png)

</div>

Na secção morada, temos a opção de definir a morada como campo único ou partida em vários campos (preferível). No caso de definirmos a morada como partida em vários campos é necessário indicar pelo menos quais os campos relativos à 'Designação' da morada e ao 'Número de porta'.

<div align="center">

![ine-plg-edif5.png](https://w.geomaster.pt/ine-plugin-edif/ine-plg-edif5.png)

</div>

## Comparação Semi-Automática
Após a configuração dos campos a utilizar na comparação podemos correr este processo. Em primeiro lugar o plugin vai executar uma comparação automática baseada na configuração efectuada. Nesta fase automática do processo a proximidade geográfica e os campos relativos à morada dos efíficios vão ser utilizados para mapear edificios entre as duas camadas.

<div align="center">

![ine-plg-edif6.png](https://w.geomaster.pt/ine-plugin-edif/ine-plg-edif6.png)

</div>

Terminado o processo de comparação automática, é adicionado um novo grupo à nossa lista de camadas, '\[INE\] Comparação'. Podemos ver este grupo na imagem em <span style="color:#6778bc">**1**</span>. As camadas presentes neste grupo correspondem ao edificado presente na camada do município em que não foi encontrada correspondência do lado do INE, `mn_nomatch_layer`; ao edificado do INE em que uma correspondência com o município foi estabelecida automaticamente, `ine_match_layer`; e por fim a uma cópia da camada de edifícios do município onde se está a adicionar o atributo que identifica o edifício nos dados do INE, `mn_map_layer`.

Após o processo de comparação automática inicia-se a etapa de associação interativa. Nesta etapa pretende-se que o utilizador estabeleça uma correspondência para cada edifício em que não foi encontrado automaticamente um mapeamento dos dados do INE. Para auxiliar esta tarefa é apresentado ao utilizador um mapa, em <span style="color:#f06e30">**2**</span>, onde será representada a geometria de cada edifício do município e os candidatos presentes nos dados do INE escolhidos com base na proximidade geográfica. Na tabela identificada em <span style="color:#008000">**3**</span> podem ser verificados os atributos relativos ao edifício do município a ser processado. De maneira a facilitar a visualização destes dados o utilizador pode também adicionar outras camadas ao mapa através do botão identificado em <span style="color:#00ffff">**4**</span>.

<div align="center">

![ine-plg-edif7.png](https://w.geomaster.pt/ine-plugin-edif/ine-plg-edif7.png)

</div>

Para possibilitar a escolha do edifício do INE que corresponde ao edifício do município a ser processado o utilizador tem a possibilidade de inspecionar os atributos de cada candidato possível. Para tal o utilizador deve primeiro selecionar o edifício do INE a inspecionar no quadro evidenciado em <span style="color:#00ff00">**5**</span> e clicar no botão 'Ver Atributos Candidato' (<span style="color:#ff0000">**6**</span>).

<div align="center">

![ine-plg-edif8.png](https://w.geomaster.pt/ine-plugin-edif/ine-plg-edif8.png)

</div>

No ecrã resultante é possível comparar os atributos de ambos os edificios (em <span style="color:#0000ff">**8**</span> e <span style="color:#ffe900">**9**</span>) de maneira a perceber se estes são de facto correspondentes. Após analisar os atributos podemos continuar o processamento clicando no botão 'Voltar' (<span style="color:#a6a6a6">**10**</span>).

De volta ao ecrã de comparação, podemos agora fazer a escolha em relação ao edifício que estamos a processar. Esta escolha está representada no conjunto de botões presentes em <span style="color:#800080">**7**</span>. Caso se tenha encontrado na lista de candidatos do INE o edifício correspondente devemos selecioná-lo em <span style="color:#00ff00">**5**</span> e clicar no botão 'Associar'; se se tratar de um novo edifício que não faça parte dos dados do INE clicar no botão 'Novo'; se os anteriores casos não se aplicarem, clicar no botão 'Saltar'.

Terminado o processo de comparação assistida temos no projeto camadas com a informação necessária para mapear o edificado municipal com os identificadores do INE, destacando-se para o município a camada `mn_map_layer` que partilha a estrutura base dos dados do município. Por fim é apresentado um dialogo onde vamos ter a possibilidade de fazer upload destes dados para o INE.
